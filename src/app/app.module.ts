import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

import { MyApp } from './app.component';



import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
// import { CommonFeaturesModule } from '../directives/common.module';
import { DatePickerDirective } from '../directives/date-picker/date-picker';
import { CommonserviceProvider } from '../providers/commonservice/commonservice';


@NgModule({
  declarations: [
    MyApp,
  
    DatePickerDirective

  ],
  imports: [
    BrowserModule,
  //CommonFeaturesModule,
    IonicModule.forRoot(MyApp),

  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
 
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    CommonserviceProvider
  ]
})
export class AppModule {}
