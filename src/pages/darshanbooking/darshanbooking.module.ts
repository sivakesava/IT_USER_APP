import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DarshanbookingPage } from './darshanbooking';
//import { DatepickerDirective } from '../../directives/datepicker.directive';

@NgModule({
  declarations: [
    DarshanbookingPage,
 //    DatepickerDirective
  ],
  imports: [
    IonicPageModule.forChild(DarshanbookingPage),
  ],
  entryComponents:[
    DarshanbookingPage
  ]
})
export class DarshanbookingPageModule {}
