import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';


@IonicPage()
@Component({
  selector: 'page-darshanbooking',
  templateUrl: 'darshanbooking.html',
})
export class DarshanbookingPage {
  toConfig: any;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    //this.toConfig = { min: [new Date().getFullYear(), new Date().getMonth(), new Date().getDate()] };
    this.setUpDate();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DarshanbookingPage');
  //  this.setUpDate()
  }
  setUpDate() {
    let currentDate = new Date();
   // this.fromConfig = { min: [currentDate.getFullYear(), currentDate.getMonth(), currentDate.getDate()] };
    this.toConfig = { min: [currentDate.getFullYear(), currentDate.getMonth(), currentDate.getDate()] };
  }
}
