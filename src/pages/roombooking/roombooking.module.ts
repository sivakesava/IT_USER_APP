import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RoombookingPage } from './roombooking';
// import { DatepickerDirective } from '../../directives/datepicker.directive';

@NgModule({
  declarations: [
    RoombookingPage,
    //DatepickerDirective
  ],
  imports: [
    IonicPageModule.forChild(RoombookingPage),
  ],
  entryComponents:[
    RoombookingPage
  ]
})
export class RoombookingPageModule {}
