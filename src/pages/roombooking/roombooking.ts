import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';



@IonicPage()
@Component({
  selector: 'page-roombooking',
  templateUrl: 'roombooking.html',
})
export class RoombookingPage {
  fromConfig: any;
  toConfig: any;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.setUpDate();
  }



  ionViewDidLoad() {
    console.log('ionViewDidLoad RoombookingPage');
  }

  setUpDate() {
    let currentDate = new Date();
    this.fromConfig = { min: [currentDate.getFullYear(), currentDate.getMonth(), currentDate.getDate()] };
    this.toConfig = { min: [currentDate.getFullYear(), currentDate.getMonth(), currentDate.getDate()] };
  }

}
