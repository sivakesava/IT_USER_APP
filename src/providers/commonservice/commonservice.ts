import { HttpClient,HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs';

/*
  Generated class for the CommonserviceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class CommonserviceProvider {
  BookingRoom(arg0: any, arg1: any): any {
    throw new Error("Method not implemented.");
  }
  query: any;
  details: any;
  headers: HttpHeaders;
  
  
  constructor(public http: HttpClient) {
    this.headers = new HttpHeaders({
      "Content-Type": "application/json", 
      "Accept": "application/json",
  });
  
  
    console.log('Hello RestProvider Provider');
  }

  //get the token
  displayName = "";
  getAuthorizationHeader(): any {
    let headers = new HttpHeaders();
    var authToken =   localStorage.getItem("authToken");
    headers = headers.set('Authorization', authToken);
    console.log(headers);
    return headers;
  }
  setToken(token, userName) {
    localStorage.setItem("authToken", token);
  }
  
  
  Logindetails(url, data): Observable<any> {
    let userData;
    return this.http.post(url, data)
      .map(data => {
        userData=data;
        console.log(data);
        if(userData.user)
        {
          localStorage.setItem("authToken", userData.user.token);
          localStorage.setItem("location", userData.user.location);

        }

        return data;
      });
  }

  CreateRegiser(url, data): Observable<any> {
   // let headers = this.getAuthorizationHeader();
    return this.http.post(url, data)
      .map(res => {
        return res;
      })
  }
  BookingRooms(url, data): Observable<any> {
    let headers = this.getAuthorizationHeader();
    return this.http.post(url, data,{ headers: headers })
      .map(res => {
        return res;
      });
  }

  getDarshanTypes(url): Observable<any>{
    let headers = this.getAuthorizationHeader();
    return this.http.get(url,{ headers: headers })
    .map( res => {
      return res;
    })
  }

  createBookingRequest(url, data): Observable<any>{

    let headers = this.getAuthorizationHeader();
    return this.http.post(url, data,{ headers: headers })
    .map( res => {
      return res;
    })
  }


  postUpload(url, data): Observable<any> {
    return this.http.post(url, data)
    .map(data=>{
      return data;
    })
  }

}
