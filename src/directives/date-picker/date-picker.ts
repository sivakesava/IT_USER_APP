import { Directive, ElementRef, Input, OnInit } from '@angular/core';

/**
 * Generated class for the DatePickerDirective directive.
 *
 * See https://angular.io/api/core/Directive for more info on Angular
 * Directives.
 */
declare var $: any;
@Directive({
  selector: '[datepicker]'
})
export class DatePickerDirective {

  @Input()
  dateParams: any
  constructor(private el: ElementRef) {

  }
  ngOnInit() {

    $(this.el.nativeElement).pickadate(this.dateParams);
  }

}
