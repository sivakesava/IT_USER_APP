import { NgModule } from '@angular/core';
import { DatePickerDirective } from './date-picker/date-picker';
@NgModule({
	declarations: [DatePickerDirective],
	imports: [],
	exports: [DatePickerDirective]
})
export class DirectivesModule {}
