import { NgModule } from '@angular/core';
import { DatepickerDirective } from './datepicker.directive';
@NgModule({
    declarations: [
        DatepickerDirective
    ],
    exports: [
        DatepickerDirective
    ]
})

export class CommonFeaturesModule { }