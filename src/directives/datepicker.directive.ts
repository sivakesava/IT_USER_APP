import {Directive, ElementRef, Input, OnInit} from '@angular/core';
declare var $:any;
@Directive({
    selector:'[datepicker]'
})

export class DatepickerDirective implements OnInit{
    @Input()
    dateParams:any
    constructor(private el:ElementRef){
        
    }
 ngOnInit(){
  
    $(this.el.nativeElement).pickadate(this.dateParams);
 }
    
}